import express from 'express';
import { fileURLToPath } from 'url';
import bodyParser from 'body-parser'; // Importa bodyParser correctamente

import path from 'path';
import router from './router/index.js';

const puerto = 80;
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const main = express();
main.set('view engine', 'ejs');
main.use(express.static(`${__dirname}/public`));

// Configura el middleware de análisis del cuerpo antes de las rutas
main.use(bodyParser.urlencoded({ extended: true }));
main.use(bodyParser.json());

// Monta el enrutador
main.use(router);

main.listen(puerto, () => {
  console.log(`Servidor corriendo en http://localhost:${puerto}`);
});
