document.addEventListener('DOMContentLoaded', function () {
    var formulario = document.getElementById('formulario-cotizacion');
    var btnCalcular = document.getElementById('btn-calcular');

    btnCalcular.addEventListener('click', function (event) {
        event.preventDefault(); // Prevenir el envío del formulario por defecto

        // Aquí puedes colocar el código para calcular
        // Por ejemplo:
        // Obtener valores de los campos
        var valorAutomovil = document.getElementById('valor').value;
        var porcentajeInicial = document.getElementById('pinicial').value;
        
        // Realizar el cálculo
        var montoInicial = (valorAutomovil * porcentajeInicial) / 100;

        // Mostrar el resultado (aquí puedes decidir qué hacer con el resultado)
        alert('El monto del pago inicial es: ' + montoInicial);
    });
    
});
